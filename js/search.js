function SearchCtrl($scope, $http) {
	/**
	 * Scope variables for storing the favourite items.
	 * Favourite Artist items
	 */
	$scope.favArtist = {
        items: []
    };
	//Favourite Album items
	$scope.favAlbum = {
        items: []
    };
	//Favourite Track itmems
	$scope.favTrack = {
        items: []
    };

	/**
	 * This Scope function will be executed on change Artist keyword in search search.
	 */
	$scope.keyword_change = function(){
		//HTTP request to Spotify search for Artist infomration
		//On Success it will update the result in scope variable $scope.result
		//On Failure it will update the error info in scope variable $scope.data
		$http({method: 'GET', url: 'https://api.spotify.com/v1/search',params:{q: $scope.keywords, type: 'artist'}}).
		success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available
			$scope.data = data;	
			$scope.result = data.artists.items;
			console.log('Fav',data.artists.items);
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.error(err);
			$scope.data = data || "Request failed";
			$scope.status = status;	
		});
	};

	/**
	 * This Scope function will result the Albums based on the Artist passed.
	 * @param {Object} Artist 
	 */
	$scope.chk_album = function(item){
		//HTTP request to Spotify search for Album infomration based on Artist
		//On Success it will update the result in scope variable $scope.result
		//On Failure it will update the error info in scope variable $scope.data
		$http({method: 'GET', url: 'https://api.spotify.com/v1/artists/'+item.id+'/albums'}).
		success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available
			$scope.data = data;	
			$scope.albums = data.items;
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.error(err);
			$scope.data = data || "Request failed";
			$scope.status = status;	
		});
	};

	/**
	 * This Scope function will result the Tracks based on the Album id passed.
	 * @param {Object} Album 
	 */
	$scope.chk_track = function(item){
		//HTTP request to Spotify search for Tracksinfomration based on Albums
		//On Success it will update the result in scope variable $scope.result
		//On Failure it will update the error info in scope variable $scope.data
		$http({method: 'GET', url: 'https://api.spotify.com/v1/albums/'+item.id+'/tracks'}).
		success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available
			$scope.data = data;	
			$scope.tracks = data.items;
		}).
		error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.error(err);
			$scope.data = data || "Request failed";
			$scope.status = status;	
		});
	};

	/**
	 * This Scope function will add the passed info to Favourites.
	 * @param {Object} Album 
	 */
	$scope.add_favourite = function(item,type){
		//Check for the type value whether it is Artist or Album or Track
		if(type=="artist"){
			$scope.favArtist.items.push({
				id: item.id,
				name: item.name,
				image: item.images[0].url
			});
		}else if(type=="album"){
			$scope.favAlbum.items.push({
				id: item.id,
				name: item.name,
				image: item.images[0].url
			});
		}else if(type="track"){
			$scope.favTrack.items.push({
				id: item.id,
				name: item.name
			});
		}
		alert("Added to Favourites");
		//console.log('Fav', $scope.favArtist.items);
	};

	/**
	 * This Scope function will remove the passed info from Favourites.
	 * @param {Object} Album 
	 */
	$scope.remove_favourite = function(type){
		//Check for the type value whether it is Artist or Album or Track
		if(type=="artist"){
			for(var i = $scope.favArtist.items.length - 1; i >= 0; i--){
				$scope.favArtist.items.splice(i,1);
			}
		}else if(type=="album"){
			for(var i = $scope.favAlbum.items.length - 1; i >= 0; i--){
				$scope.favAlbum.items.splice(i,1);
			}
		}else if(type="track"){
			for(var i = $scope.favTrack.items.length - 1; i >= 0; i--){
				$scope.favTrack.items.splice(i,1);
			}
		}
	};
}
